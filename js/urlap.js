//var form;
function regForm(){
    form = document.getElementsByTagName("input");
    return form;
}
function userCheck(){
    var userName = document.getElementById("username").value;
    userName = userName.trim();
    document.getElementById('userNameError').innerHTML = "";
    if (userName.length > 7 && userName.length < 26){
       if (isFinite(userName[0])) {
           document.getElementById('userNameError').innerHTML = "Nem kezdődhet számmal!";
       }
    } else {
        document.getElementById('userNameError').innerHTML = "8 és 25 karakter közöttinek kell lennie, vagy számmal kezdődik!";
    }
    //console.log(typeof(userName));
}
function userCheck2(urlap){
    console.log(urlap[0].value);
}
function pwdCheck(){
    document.getElementById("pwdError").innerHTML ="";
    let pwd = document.getElementById("password").value;
    let pwdlength = pwd.length;
    //console.log(typeof(document.getElementById("password")).value);
    //console.log(pwdlength);
    if (pwdlength > 7){
        if (!(vanEBenneSzam(pwd))){
            //A jelszóban nincs szám
            document.getElementById("pwdError").innerHTML ="A jelszónak számot is kell tartalmaznia!";
        }
        if (!kisNagyBetu(pwd)){
            document.getElementById("pwdError").innerHTML +=" A jelszónak kis- és nagybetűt is kell tartalmaznia!";
        }
    }
}

function kisNagyBetu(szo){
    if (szo == szo.toLowerCase() || szo == szo.toUpperCase()) {
        return false;
    }
    return true;
}

function vanEBenneSzam(szo){
    let hossz = szo.length;
    for (let i = 0; i < hossz; i++){
        if (isFinite(szo.charAt(i))){
            return true;
        }
    }
    return false;
}

function pwdEqual(){
    document.getElementById("pwdEqualError").innerHTML = "";
    let pwd = document.getElementById("password").value;
    let pwdc = document.getElementById("passwordconfirm").value;
    let equal = false;
    if (pwd == pwdc) {
        equal = true;
    } else {
        document.getElementById("pwdEqualError").innerHTML = "A két jelszó nem egyezik meg!";
    }
    
}
